﻿using SkySeekers.AbilitySystem;

public class SigilAbility
{
    public SigilAbilityTemplate Template { get; private set; }
    public AbilityTemplate Ability { get; private set; }
    public bool IsOffCooldown { get; private set; } = true;

    public SigilAbility(SigilAbilityTemplate template)
    {
        Template = template;
        Ability = template.Ability;
    }
}
