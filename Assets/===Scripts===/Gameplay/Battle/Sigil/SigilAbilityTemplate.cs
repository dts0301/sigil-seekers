﻿using SkySeekers.AbilitySystem;
using UnityEngine;

[CreateAssetMenu(menuName = "Sigil/AbilityTemplate")]
public class SigilAbilityTemplate : ScriptableObject
{
    [SerializeField] private AbilityTemplate template;
    public AbilityTemplate Ability => template;

    [Space]
    [SerializeField] private int apCost = 1;
    public int APCost => apCost;

    [SerializeField] private SigilType[] requirement;
    public SigilType[] Requirement => requirement;

    [Space]
    [SerializeField] private SigilAbilityType type;
    public SigilAbilityType Type => type;

    public bool FindAndHighlightSigils(SigilHand hand)
    {
        return hand?.Find(requirement, true) ?? true;
    }

    public void UnhighlightSigils(SigilHand hand)
    {
        hand?.Unhighlight();
    }

    public bool ConsumeSigils(SigilHand hand)
    {
        if (hand == null)
        {
            return true;
        }

        if (hand.Find(requirement))
        {
            foreach (SigilType type in requirement)
            {
                hand.DestroyFirst(type);
            }
            return true;
        }
        return false;
    }
}
