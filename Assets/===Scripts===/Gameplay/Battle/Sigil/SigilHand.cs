﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Sigil/SigilHand")]
public class SigilHand : ScriptableObject
{
    [SerializeField] private Sigil cardPrefab;
    [SerializeField] private List<SigilType> types;
    [SerializeField] private int handLimit;
    [SerializeField] private int defaultDrawAmount;
    [SerializeField] private float drawSigilsInterval;

    private int sigilsCount = 0;
    private Dictionary<SigilType, List<Sigil>> sigils = new Dictionary<SigilType, List<Sigil>>();
    private HashSet<Sigil> highlighted = new HashSet<Sigil>();
    private HashSet<Sigil> search = new HashSet<Sigil>();
    private Transform sigilsTransform;

    public int DrawAmount { get; set; }

    public event UnityAction<SigilHand> OnChange;

    public void Initialize(Transform sigilsTransform)
    {
        this.sigilsTransform = sigilsTransform;
        DrawAmount = defaultDrawAmount;
        sigilsCount = 0;
        sigils.Clear();
        highlighted.Clear();
        search.Clear();
    }

    public void DrawSigils()
    {
        TimeManager.Instance.StartCoroutine(DrawRandomSigilsCoroutine());
    }

    private IEnumerator DrawRandomSigilsCoroutine()
    {
        for (int i = 0; i < DrawAmount; i++)
        {
            SigilType randomType = types[Random.Range(0, types.Count)];
            CreateCard(randomType);
            yield return new WaitForSeconds(drawSigilsInterval);
        }
    }

    public void CreateCard(SigilType type)
    {
        if (sigilsCount < handLimit)
        {
            Sigil newCard = Instantiate(cardPrefab, sigilsTransform);
            newCard.Type = type;

            List<Sigil> typeSigils;
            if (sigils.TryGetValue(type, out typeSigils))
            {
                typeSigils.Add(newCard);
            }
            else
            {
                sigils.Add(type, new List<Sigil>());
                sigils[type].Add(newCard);
            }
            sigilsCount++;
            OnChange?.Invoke(this);
        }
    }
    
    public bool Find(SigilType[] types, bool highlight = false)
    {
        search.Clear();
        for (int i = 0; i < types.Length; i++)
        {
            List<Sigil> typeSigils;
            if (sigils.TryGetValue(types[i], out typeSigils))
            {
                bool found = false;
                for (int j = 0; j < typeSigils.Count; j++)
                {
                    if (!search.Contains(typeSigils[j]))
                    {
                        found = true;
                        search.Add(typeSigils[j]);
                        break;
                    }
                }
                if (!found)
                {
                    search.Clear();
                    return false;
                }
            }
            else
            {
                sigils[types[i]] = new List<Sigil>();
                return false;
            }
        }
        if (highlight)
        {
            Highlight();
        }
        return true;
    }

    public void DestroyFirst(SigilType type)
    {
        List<Sigil> typeSigils;
        if (sigils.TryGetValue(type, out typeSigils))
        {
            if (typeSigils.Count > 0)
            {
                Sigil toDestroy = typeSigils[0];

                toDestroy.Highlighted = false;
                typeSigils.Remove(toDestroy);
                highlighted.Remove(toDestroy);
                search.Remove(toDestroy);

                Destroy(toDestroy.gameObject);
                sigilsCount--;

                OnChange?.Invoke(this);
            }
        }
        else
        {
            sigils[type] = new List<Sigil>();
        }
    }

    private void Highlight()
    {
        Unhighlight();
        foreach (Sigil s in search)
        {
            highlighted.Add(s);
        }
        foreach (Sigil s in highlighted)
        {
            s.Highlighted = true;
        }
    }

    public void Unhighlight()
    {
        foreach (Sigil s in highlighted)
        {
            s.Highlighted = false;
        }
        highlighted.Clear();
    }
}
