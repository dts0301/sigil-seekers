﻿using SkySeekers.AbilitySystem;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Sigil/AbilityType")]
public class SigilAbilityType : ScriptableObject
{
    [SerializeField] private bool selectFriendlies, selectEnemies, preferThreatening, preferThreatened;

    private struct ThreatRandomizedUnit
    {
        public GameUnit Unit;
        public float Level;
    }

    public List<AbilitySystemAgent> GetPreferredTargets(GameUnit caster, float threatDeviation)
    {
        List<GameUnitThreatHandler.RandomizedThreatLevels> targets = new List<GameUnitThreatHandler.RandomizedThreatLevels>();

        if (selectFriendlies)
        {
            foreach (var unit in caster.Party.Members)
            {
                targets.Add(unit.ThreatHandler.GetRandomizedLevels(threatDeviation));
            }
        }
        if (selectEnemies)
        {
            foreach (var unit in caster.Party.EnemyParty.Members)
            {
                targets.Add(unit.ThreatHandler.GetRandomizedLevels(threatDeviation));
            }
        }

        targets.Shuffle();

        if (preferThreatening && preferThreatened)
        {
            targets.Sort((a, b) =>
            {
                return b.Threat != a.Threat ? b.Threat.CompareTo(a.Threat) : b.Threatened.CompareTo(a.Threatened);
            });
        }
        else if (preferThreatening)
        {
            targets.Sort((a, b) =>
            {
                return b.Threat.CompareTo(a.Threat);
            });
        }
        else if (preferThreatened)
        {
            targets.Sort((a, b) =>
            {
                return b.Threatened.CompareTo(a.Threatened);
            });
        }

        return targets.Select(t => t.Unit.AbilitySystemAgent).ToList();
    }
}