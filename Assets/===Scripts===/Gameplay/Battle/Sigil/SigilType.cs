﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sigil/SigilType")]
public class SigilType : ScriptableObject
{
    [SerializeField] private Sprite icon;
    public Sprite Icon { get => icon; }
}