﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Sigil : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private List<Color> colorLayers = new List<Color>();

    [SerializeField] private Image icon;
    [SerializeField] private Color highlightColor;
    [SerializeField] private Color focusColor;
    [SerializeField] private Color inactiveColor;

    private SigilType type;
    public SigilType Type { get => type; set { type = value; icon.sprite = type.Icon; } }

    private bool highlighted;
    public bool Highlighted
    {
        get => highlighted;

        set
        {
            highlighted = value;
            if (highlighted)
            {
                colorLayers.Add(highlightColor);
                icon.color = highlightColor;
            }
            else
            {
                colorLayers.Remove(highlightColor);
                icon.color = colorLayers.Count == 0 ? Color.black : colorLayers[colorLayers.Count - 1];
            }
        }
    }

    private bool focused;
    public bool Focused
    {
        get => focused;

        set
        {
            focused = value;
            if (focused)
            {
                colorLayers.Add(focusColor);
                icon.color = focusColor;
            }
            else
            {
                colorLayers.Remove(focusColor);
                icon.color = colorLayers.Count == 0 ? Color.black : colorLayers[colorLayers.Count - 1];
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Focused = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Focused = false;
    }
}
