﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Utility/GameUnitVariable")]
public class GameUnitSOVariable : ObservedSOVariable<GameUnit> { }

[System.Serializable]
public class GameUnitEvent : UnityEvent<GameUnit> { }