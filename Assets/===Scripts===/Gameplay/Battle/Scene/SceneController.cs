﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] private Party[] parties;
    [Space]
    [SerializeField] private BlackMask blackMask;
    [SerializeField] private CanvasGroup blockInteraction;
    [SerializeField] private Animator battleTextAnimator;
    [SerializeField] private Animator battleUiAnimator;
    [SerializeField] private TurnController turnController;

    private void Start()
    {
        StartCoroutine(IntroSequence());
    }

    public IEnumerator IntroSequence()
    {
        yield return null;

        SpawnCharacters();

        yield return new WaitForSeconds(1f);

        blackMask.FadeIn();

        yield return new WaitForSeconds(1.2f);

        battleTextAnimator.SetTrigger("Intro");

        yield return new WaitForSeconds(1.5f);

        EnterCharacters();

        yield return new WaitForSeconds(.8f);
        blockInteraction.enabled = false;
        battleUiAnimator.SetTrigger("Appear");
        turnController.Initialize(parties);
        turnController.StartRound();
    }

    public void SpawnCharacters()
    {
        for (int i = 0; i < parties.Length; i++)
        {
            parties[i].Spawn();
        }
    }

    public void EnterCharacters()
    {
        for (int i = 0; i < parties.Length; i++)
        {
            parties[i].EnterCharacters();
        }
    }

    public void Victory()
    {
        blockInteraction.enabled = true;
        battleTextAnimator.SetTrigger("Victory");
        StartCoroutine(BattleOverSequence());
    }

    public void Defeat()
    {
        blockInteraction.enabled = true;
        battleTextAnimator.SetTrigger("Defeat");
        StartCoroutine(BattleOverSequence());
    }

    public IEnumerator BattleOverSequence()
    {
        yield return new WaitForSeconds(2f);

        blackMask.FadeOut(() => SceneManager.LoadScene("Menu"));
    }

}
