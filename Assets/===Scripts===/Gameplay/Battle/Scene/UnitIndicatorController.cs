﻿using UnityEngine;

public class UnitIndicatorController : MonoBehaviour
{
    [SerializeField] private GameUnitSOVariable selectedCharacter;
    [SerializeField] private GameUnitSOVariable activeCharacter;
    [SerializeField] private Transform selectedIndicator, activeIndicator;

    private void Start()
    {
        selectedCharacter.AddPostListener(UpdateSelected);
        activeCharacter.AddPostListener(UpdateActive);
    }

    public void UpdateActive()
    {
        GameUnit unit = activeCharacter.Value;
        activeIndicator.SetParent(unit.transform);
        activeIndicator.localPosition = Vector3.zero;
    }

    public void UpdateSelected()
    {
        GameUnit unit = selectedCharacter.Value;
        selectedIndicator.SetParent(unit.transform);
        selectedIndicator.localPosition = Vector3.zero;
    }
}
