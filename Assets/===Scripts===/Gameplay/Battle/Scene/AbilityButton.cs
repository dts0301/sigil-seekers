﻿
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Party playerParty;
    [SerializeField] private GameUnitSOVariable selectedChar;
    [SerializeField] private TextMeshProUGUI textMesh;
    [SerializeField] private int abilityIndex;

    private SigilAbility ability;
    private Button button;

    private bool haveAP;
    private bool haveSigils;
    private bool unitActive;

    private void Start()
    {
        button = GetComponent<Button>();

        selectedChar.AddPreListener(RemoveListeners);
        selectedChar.AddPostListener(UpdateInfo);
    }

    public void CastAbility()
    {
        selectedChar.Value.CastAbility(ability);
    }

    private void UpdateInfo()
    {
        GameUnit unit = selectedChar.Value;
        if (ButtonVisible(unit))
        {
            gameObject.SetActive(true);

            ability = unit.Abilities[abilityIndex];
            textMesh.text = ability.Ability.name;

            unit.APHandler.OnAPChange += CheckAP;
            if (unit.Party.SigilHand != null)
            {
                unit.Party.SigilHand.OnChange += CheckSigils;
            }
            unitActive = unit.Active;
            haveAP = unit.APHandler.CurrentAP >= ability.Template.APCost;
            haveSigils = unit.Party.SigilHand?.Find(ability.Template.Requirement) ?? true;
            CheckInteractable();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void RemoveListeners()
    {
        GameUnit unit = selectedChar.Value;
        if (unit != null)
        {
            unit.APHandler.OnAPChange -= CheckAP;
            if (unit.Party.SigilHand != null)
            {
                unit.Party.SigilHand.OnChange -= CheckSigils;
            }
        }
    }

    private void CheckAP()
    {
        float current = selectedChar.Value.APHandler.CurrentAP;
        haveAP = current >= ability.Template.APCost;
        CheckInteractable();
    }

    private void CheckSigils(SigilHand hand)
    {
        haveSigils = hand?.Find(ability.Template.Requirement) ?? true;
        CheckInteractable();
    }

    private bool ButtonVisible(GameUnit unit)
    {
        return !unit.IsAI &&
            unit.Party == playerParty &&
            abilityIndex < unit.Abilities.Count &&
            unit.Abilities[abilityIndex] != null;
    }

    private void CheckInteractable()
    {
        button.interactable = haveAP && haveSigils && unitActive;
    }

    private void Unhighlight()
    {
        ability.Template.UnhighlightSigils(selectedChar.Value.Party.SigilHand);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!selectedChar.Value.AbilitySystemAgent.AbilityHandler.Casting)
        {
            ability.Template.FindAndHighlightSigils(selectedChar.Value.Party.SigilHand);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!selectedChar.Value.AbilitySystemAgent.AbilityHandler.Casting)
        {
            Unhighlight();
        }
    }
}
