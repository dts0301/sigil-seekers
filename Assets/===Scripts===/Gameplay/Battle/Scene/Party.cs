﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Party : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Transform[] partyPositions;

    [Space]
    [SerializeField] private PartyTemplate party;
    public PartyTemplate PartyTemplate => party;

    [SerializeField] Party enemyParty;
    public Party EnemyParty => enemyParty;

    [Space]
    [SerializeField] int sigilDrawAmount = 3;
    public int SigilDrawAmount => sigilDrawAmount;

    [SerializeField] SigilHand hand;
    public SigilHand SigilHand => hand;

    [SerializeField] bool isAIParty;
    public bool IsAIParty => isAIParty;

    [SerializeField] bool flipSprites;
    public bool FlipSprites => flipSprites;

    [SerializeField] UnityEvent onWipe;
    public UnityEvent OnWipe => OnWipe;

    public List<GameUnit> Members { get; private set; } = new List<GameUnit>();
    
    public void Spawn()
    {
        foreach (Character unit in party.Members)
        {
            Character newChar = Instantiate(unit, transform);
            newChar.transform.position = spawnPoint.position;
            Members.Add(newChar.GameUnit);
            newChar.GameUnit.AbilitySystemAgent.HealthHandler.OnDeathChange += CheckWipe;
            newChar.GameUnit.Initialize(this);
            if (isAIParty)
            {
                newChar.GameUnit.IsAI = true;
            }
        }
    }

    private void CheckWipe(bool dead)
    {
        if (dead)
        {
            for (int i = 0; i < Members.Count; i++)
            {
                if (!Members[i].AbilitySystemAgent.HealthHandler.IsDead)
                {
                    return;
                }
            }
            onWipe.Invoke();
        }
    }

    public void EnterCharacters()
    {
        for (int i = 0; i < Members.Count; i++)
        {
            Members[i].Character.GetComponent<CharacterMover>().Destination = partyPositions[i].position;
        }
    }
}
