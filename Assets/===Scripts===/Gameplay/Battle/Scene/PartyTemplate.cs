﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PartyTemplate : ScriptableObject
{
    [SerializeField] private List<Character> characters;
    public List<Character> Members => characters;
}
