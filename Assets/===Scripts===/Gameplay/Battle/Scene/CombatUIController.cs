﻿
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CombatUIController : MonoBehaviour
{
    [SerializeField] private Party playerParty;
    [SerializeField] private GameUnitSOVariable selectedCharacter;
    [Space]
    [SerializeField] private Animator unitPanelAnim;
    [SerializeField] private TextMeshProUGUI charNameMesh;
    [SerializeField] private TextMeshProUGUI hpMesh;
    [SerializeField] private TextMeshProUGUI apMesh;
    [SerializeField] private SigilHand playerSigilHand;
    [SerializeField] private Transform playerSigilsPanel;
    [SerializeField] private Button endTurnButton;
    [Space]
    [SerializeField] private SigilType[] initialSigilHand;
    
    private void Start()
    {
        selectedCharacter.AddPostListener(UpdateName);
        selectedCharacter.AddPreListener(() => 
        {
            GameUnit unit = selectedCharacter.Value;
            if (unit != null)
            {
                unit.AbilitySystemAgent.HealthHandler.OnHealthChange -= UpdateHp;
                UpdateHp();
            }
        });
        selectedCharacter.AddPostListener(() => 
        {
            GameUnit unit = selectedCharacter.Value;
            if (unit != null)
            {
                unit.AbilitySystemAgent.HealthHandler.OnHealthChange += UpdateHp;
                UpdateHp();
            }
        });
        selectedCharacter.AddPreListener(() =>
        {
            GameUnit unit = selectedCharacter.Value;
            if (unit != null)
            {
                unit.APHandler.OnAPChange -= UpdateAp;
                UpdateAp();
            }
        });
        selectedCharacter.AddPostListener(() =>
        {
            GameUnit unit = selectedCharacter.Value;
            if (unit != null)
            {
                unit.APHandler.OnAPChange += UpdateAp;
                UpdateAp();
            }
        });
        //selectedCharacter.AddPostListener(unit => endTurnButton.interactable = unit.Party == playerParty);

        playerSigilHand.Initialize(playerSigilsPanel);
        foreach (SigilType type in initialSigilHand)
        {
            playerSigilHand.CreateCard(type);
        }
    }

    private void UpdateName()
    {
        GameUnit unit = selectedCharacter.Value;
        unitPanelAnim.SetTrigger("ChangeUnit");
        charNameMesh.text = unit.Template.UnitName;
    }

    private void UpdateHp()
    {
        float current = selectedCharacter.Value.AbilitySystemAgent.HealthHandler.Health;
        float max = selectedCharacter.Value.AbilitySystemAgent.HealthHandler.MaxHealth;
        hpMesh.text = string.Format("HP  {0}/{1}", Mathf.Ceil(current).ToString("0"), Mathf.Ceil(max).ToString("0"));
    }

    private void UpdateAp()
    {
        int current = selectedCharacter.Value.APHandler.CurrentAP;
        int max = selectedCharacter.Value.APHandler.MaxAP;
        apMesh.text = string.Format("AP  {0}/{1}", current.ToString("0"), max.ToString("0"));
    }
}
