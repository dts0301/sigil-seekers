﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickTargeting : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private LayerMask targetLayer;
    [Space]
    [SerializeField] private AbilityTargeter abilityTargeter;
    [SerializeField] private GameUnitSOVariable selectedCharacter;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit;
            if (HelperMethods.RaycastAtMouse2D(mainCamera, targetLayer, out hit))
            {
                if (abilityTargeter.IsTargeting)
                {
                    abilityTargeter.SelectTarget(hit.collider.GetComponent<Character>().GameUnit.AbilitySystemAgent);
                }
                else
                {
                    selectedCharacter.Value = hit.collider.GetComponent<Character>().GameUnit;
                }
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            abilityTargeter.CancelTargeting();
        }
    }
}
