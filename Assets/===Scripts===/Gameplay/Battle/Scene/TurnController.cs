﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu]
public class TurnController : ScriptableObject
{
    [SerializeField] private PartyTemplate playerParty;
    [SerializeField] private GameUnitSOVariable currentActiveAgent;
    [SerializeField] private GameUnitSOVariable selectedCharacter;

    [SerializeField] private UnityEvent onReset;
    public UnityEvent OnReset => onReset;

    private List<GameUnitTurnHandler> allAgents = new List<GameUnitTurnHandler>();
    private LinkedList<GameUnitTurnHandler> turnQueue = new LinkedList<GameUnitTurnHandler>();
    
    public void Initialize(Party[] parties)
    {
        currentActiveAgent.AddPreListener(() =>
        {
            GameUnit unit = currentActiveAgent.Value;
            if (unit != null)
            {
                unit.EndTurn();
            }
        });
        currentActiveAgent.AddPostListener(() =>
        {
            GameUnit unit = currentActiveAgent.Value;
            unit.StartTurn();
            unit.APHandler.CurrentAP = unit.APHandler.DefaultAP;
        });

        allAgents.Clear();
        for (int i = 0; i < parties.Length; i++)
        {
            foreach (GameUnit unit in parties[i].Members)
            {
                GameUnitTurnHandler turnHandler = unit.GetComponent<GameUnitTurnHandler>();
                turnHandler.AddChangedListener(UpdateAgentPlaceInQueue);
                allAgents.Add(turnHandler);
            }
        }
    }

    public void StartRound()
    {
        var sorted = from agent in allAgents
                     orderby agent.Initiative descending
                     select agent;

        turnQueue.Clear();
        foreach (GameUnitTurnHandler agent in sorted)
        {
            turnQueue.AddLast(agent);
        }

        if (HasNext)
        {
            Next();
        } 
    }

    public bool HasNext => turnQueue.First != null;

    public void Next()
    {
        if (turnQueue.First == null)
        {
            StartRound();
            onReset.Invoke();
        }
        else
        {
            GameUnitTurnHandler result = turnQueue.First.Value;
            while (result.Unit.AbilitySystemAgent.HealthHandler.IsDead)
            {
                turnQueue.RemoveFirst();
                result = turnQueue.First.Value;
            }
            turnQueue.RemoveFirst();
            
            currentActiveAgent.Value = result.Unit;
            if (result.Unit.Party.PartyTemplate == playerParty)
            {
                selectedCharacter.Value = result.Unit;
            }
        }
    }

    private void UpdateAgentPlaceInQueue(GameUnitTurnHandler changedAgent)
    {
        if (turnQueue.Contains(changedAgent))
        {
            turnQueue.Remove(changedAgent);

            LinkedListNode<GameUnitTurnHandler> cur = turnQueue.First;

            if (cur == null || changedAgent.Initiative <= turnQueue.Last.Value.Initiative)
            {
                turnQueue.AddLast(changedAgent);
                return;
            }

            while (cur.Value.Initiative > changedAgent.Initiative)
            {
                cur = cur.Next;
            }
            turnQueue.AddBefore(cur, changedAgent);
        }
    }
}
