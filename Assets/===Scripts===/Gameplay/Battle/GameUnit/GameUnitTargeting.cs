﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameUnit/Targeting")]
public class GameUnitTargeting : AbilityTargeting
{
    [SerializeField] private bool canSelectAllies, canSelectEnemies;

    public override bool SelectTargetCheck(AbilitySystemAgent selected, AbilityInstance abilityInstance)
    {
        GameUnit caster = abilityInstance.Caster.GetComponent<GameUnit>();
        GameUnit target = selected.GetComponent<GameUnit>();
        return base.SelectTargetCheck(selected, abilityInstance) &&
            (canSelectAllies || target.Party != caster.Party) &&
            (canSelectEnemies || target.Party == caster.Party);
    }
}
