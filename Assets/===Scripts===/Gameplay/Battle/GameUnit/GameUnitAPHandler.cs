﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameUnitAPHandler : MonoBehaviour
{
    public int DefaultAP { get; private set; }

    private int maxAP;
    public int MaxAP { get { return maxAP; } set { maxAP = value; OnAPChange?.Invoke(); } }

    private int ap;
    public int CurrentAP { get { return ap; } set { ap = value; OnAPChange?.Invoke(); } }

    public event UnityAction OnAPChange;

    public void Initialize(int defaultAP)
    {
        DefaultAP = defaultAP;
        MaxAP = DefaultAP;
    }

    public void StartAP()
    {
        CurrentAP += MaxAP;
    }
}

