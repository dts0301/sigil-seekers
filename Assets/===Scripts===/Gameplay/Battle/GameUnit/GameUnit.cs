﻿using SkySeekers.AbilitySystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameUnit : MonoBehaviour
{
    public Character Character { get; private set; }
    public GameUnitTemplate Template => Character.Template;

    public AbilitySystemAgent AbilitySystemAgent { get; private set; }
    public GameUnitTurnHandler TurnAgent { get; private set; }
    public GameUnitAPHandler APHandler { get; private set; }
    public GameUnitThreatHandler ThreatHandler { get; private set; }
    public GameUnitAIHandler AIHandler { get; private set; }

    public Party Party { get; private set; }
    public float Power { get; set; }
    public List<SigilAbility> Abilities { get; private set; } = new List<SigilAbility>();
    public bool Active { get; private set; }
    public bool IsAI { get; set; }

    private Animator anim;
    private SpriteRenderer sprite;
    private SigilAbility currentSigilAbility;
    private bool currentAbilityConsumesSigils;

    private void Awake()
    {
        Character = transform.parent.GetComponent<Character>();
        if (Character == null)
        {
            Debug.LogError("Game Unit is not child of a Character!");
        }

        AbilitySystemAgent = GetComponent<AbilitySystemAgent>();
        TurnAgent = GetComponent<GameUnitTurnHandler>();
        APHandler = GetComponent<GameUnitAPHandler>();
        ThreatHandler = GetComponent<GameUnitThreatHandler>();
        AIHandler = GetComponent<GameUnitAIHandler>();
    }

    private void Start()
    {
        SigilAbilityTemplate[] abilityTemps = Character.Template.Abilities;
        foreach (SigilAbilityTemplate abT in abilityTemps)
        {
            Abilities.Add(new SigilAbility(abT));
        }

        AbilitySystemAgent.AbilityHandler.OnCastBegin += abInst => APHandler.CurrentAP--;
        AbilitySystemAgent.AbilityHandler.OnCastBegin += abInst => ConsumeAbilitySigils();
        AbilitySystemAgent.AbilityHandler.OnCastBegin += abInst => ColorModel(true);
        AbilitySystemAgent.AbilityHandler.OnCastDone += abInst => ColorModel(false);
        AbilitySystemAgent.AbilityHandler.OnDoAction += AnimateAction;
        AbilitySystemAgent.HealthHandler.OnDeathChange += dead => { if (dead) { anim.SetTrigger("Death"); } };

        AbilitySystemAgent.HealthHandler.Initialize(Character.Template.Health);
        TurnAgent.Initialize(Character.Template.Initiative);
        APHandler.Initialize(Character.Template.DefaultAP);
        AIHandler.Initialize();
        Power = Character.Template.Power;
        ThreatHandler.Initialize(Party); // do after health and power handlers
    }

    public void Initialize(Party party)
    {
        Party = party;
        anim = Character.Model.GetComponent<Animator>();
    }

    public void StartTurn()
    {
        Active = true;
        APHandler.StartAP();
        AbilitySystemAgent.StatusHandler.TickStatuses();
        AbilitySystemAgent.StatusHandler.DecrementTimes(1);

        if (IsAI)
        {
            AIHandler.DoAITurn();
        }
    }

    public void EndTurn()
    {
        Active = false;
        APHandler.CurrentAP = 0;
    }

    public void CastAbility(SigilAbility ability)
    {
        if (APHandler.CurrentAP > 0 && ability.Template.FindAndHighlightSigils(Party.SigilHand))
        {
            currentAbilityConsumesSigils = true;
            currentSigilAbility = ability;
            AbilitySystemAgent.AbilityHandler.Cast(ability.Ability);
        }
    }

    public void ForceCastAbility(SigilAbility ability, List<AbilitySystemAgent> targets = null)
    {
        AbilitySystemAgent.AbilityHandler.Cast(ability.Ability, targets);
        currentSigilAbility = ability;
        currentAbilityConsumesSigils = false;
    }

    private void ConsumeAbilitySigils()
    {
        if (currentAbilityConsumesSigils)
        {
            currentSigilAbility.Template.ConsumeSigils(Party.SigilHand);
        }
        currentAbilityConsumesSigils = true;
    }

    public void AnimateAction(ActionInstance action)
    {
        string animationName = action.Template.AnimationName;
        if (anim != null && !animationName.Equals(""))
        {
            anim.SetTrigger(animationName);
        }
    }

    // TEST
    private void ColorModel(bool casting)
    {
        if (sprite != null)
        {
            sprite.color = casting ? Color.red : Color.blue;
        }
    }
}
