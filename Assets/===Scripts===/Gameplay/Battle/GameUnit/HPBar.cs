﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class HPBar : MonoBehaviour
{
    [SerializeField] private AbilitySystemAgent agent;
    [SerializeField] private Transform fill;
    [SerializeField] private TextMeshPro textMesh;
    
    private void Start()
    {
        agent.HealthHandler.OnHealthChange += UpdateInfo;
        UpdateInfo();
    }

    private void UpdateInfo()
    {
        float current = agent.HealthHandler.Health;
        float max = agent.HealthHandler.MaxHealth;

        fill.localScale = new Vector3(current / max, 1, 1);

        textMesh.text = string.Format("{0} / {1}", Mathf.Ceil(current).ToString("0"), Mathf.Ceil(max).ToString("0"));
    }
}
