﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameUnitTurnHandler : MonoBehaviour
{
    [SerializeField] private TurnController controller;

    public GameUnit Unit { get; private set; }

    private int initiative;
    public int Initiative
    {
        get { return initiative; }

        set
        {
            initiative = value;
            changed.Invoke(this);
        }
    }

    private TurnHandlerEvent changed = new TurnHandlerEvent();
    public void AddChangedListener(UnityAction<GameUnitTurnHandler> listener) => changed.AddListener(listener);
    public void RemoveChangedListener(UnityAction<GameUnitTurnHandler> listener) => changed.RemoveListener(listener);

    private void Awake()
    {
        Unit = GetComponent<GameUnit>();
    }

    public void Initialize(int initiative)
    {
        Initiative = initiative;
    }
}
