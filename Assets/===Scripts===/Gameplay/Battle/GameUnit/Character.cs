﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private GameUnitTemplate template;
    public GameUnitTemplate Template => template;

    [SerializeField] private GameUnit gameUnit;
    public GameUnit GameUnit => gameUnit;

    [SerializeField] private GameObject model;
    public GameObject Model => model;

    private void Start()
    {
        if (gameUnit.Party.FlipSprites)
        {
            Vector3 curScale = transform.localScale;
            Model.transform.localScale = new Vector3(curScale.x * -1, curScale.y, curScale.z);
        }
    }
}
