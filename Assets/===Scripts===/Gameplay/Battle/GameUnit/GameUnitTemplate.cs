﻿using SkySeekers.AbilitySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameUnit/Template")]
public class GameUnitTemplate : ScriptableObject
{
    [SerializeField] private string unitName;
    public string UnitName => unitName;

    [SerializeField] private float health;
    public float Health => health;

    [SerializeField] private float power;
    public float Power => power;

    [SerializeField] private List<SigilAbilityTemplate> abilities;
    public SigilAbilityTemplate[] Abilities => abilities.ToArray();

    [SerializeField] private int initiative;
    public int Initiative => initiative;

    [SerializeField] private int defaultAP = 2;
    public int DefaultAP => defaultAP;

    [SerializeField] private GameObject characterPrefab; 
    public GameObject CharacterPrefab => characterPrefab;

    [SerializeField] private GameUnitAIController aiController;
    public GameUnitAIController AIController => aiController;

}
