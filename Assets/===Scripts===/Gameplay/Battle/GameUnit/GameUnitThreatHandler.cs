﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameUnitThreatHandler : MonoBehaviour
{
    public struct RandomizedThreatLevels
    {
        public GameUnit Unit;
        public float Threat, Threatened;
    }

    [SerializeField]
    private float maxHealthThreatenedScale = 2f;
    [SerializeField]
    private float curHealthThreatenedScale = 1f;

    public float ThreatLevel { get; set; } = 0f;
    public float ThreatenedLevel { get; set; } = 0f;
    public bool IsThreatening => ThreatLevel > 130f;
    public bool IsThreatened => ThreatenedLevel > (unit.AbilitySystemAgent.HealthHandler.MaxHealth / 2);

    private GameUnit unit;
    private float previousHealth;

    private void Awake()
    {
        unit = GetComponent<GameUnit>();
    }

    public void Initialize(Party party)
    {
        unit.AbilitySystemAgent.HealthHandler.OnHealthChange += () => 
        {
            float currentHealth = unit.AbilitySystemAgent.HealthHandler.Health;
            ThreatenedLevel += curHealthThreatenedScale * (currentHealth - previousHealth);
            previousHealth = currentHealth;
        };

        ThreatenedLevel += maxHealthThreatenedScale * 
            (party.Members.Select(m => m.AbilitySystemAgent.HealthHandler.MaxHealth).Max() - unit.AbilitySystemAgent.HealthHandler.MaxHealth);

        ThreatLevel = unit.Power;
    }

    public RandomizedThreatLevels GetRandomizedLevels(float deviation = 0f)
    {
        var result = new RandomizedThreatLevels();
        result.Unit = unit;
        result.Threat = Random.Range(ThreatLevel - deviation, ThreatLevel + deviation);
        result.Threatened = Random.Range(ThreatenedLevel - deviation, ThreatenedLevel + deviation);
        return result;        
    }
}
