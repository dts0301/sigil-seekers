﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour
{
    private Vector3 destination;
    public Vector3 Destination { get { return destination; } set { destination = value; moving = true; } }

    private float moveSpeed = 5f;
    private bool moving;

    private void Update()
    {
        if (moving)
        {
            if (Vector3.Distance(transform.position, destination) > 0.1f)
            {
                transform.position = Vector3.Lerp(transform.position, destination, moveSpeed * Time.deltaTime);
            }
            else
            {
                moving = false;
            }
        }
    }
}
