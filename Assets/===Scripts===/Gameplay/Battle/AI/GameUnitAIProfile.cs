﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "AI/Profile")]
public class GameUnitAIProfile : ScriptableObject
{
    [SerializeField] private List<AbilityTypePriority> abilityPriorities;
    [SerializeField] private float threatDeviation;

    List<SigilAbilityType> validTypes = new List<SigilAbilityType>();

    [System.Serializable]
    private struct AbilityTypePriority
    {
        public SigilAbilityType Type;
        [Range(0f, 1f)] public float ChanceToSelect;
    }

    public bool DoAction(GameUnit unit)
    {
        if (unit.APHandler.CurrentAP < 1)
        {
            return false;
        }

        SigilAbilityType type = SelectAbilityType(unit);
        if (type == null)
        {
            return false;
        }
        SigilAbility ability = SelectAbility(unit, type);
        
        unit.ForceCastAbility(ability, ability.Template.Type.GetPreferredTargets(unit, threatDeviation));
        unit.APHandler.CurrentAP--;
        return true;
    }

    private SigilAbilityType SelectAbilityType(GameUnit unit)
    {
        validTypes.Clear();
        for (int i = 0; i < abilityPriorities.Count; i++)
        {
            AbilityTypePriority ap = abilityPriorities[i];
            if (unit.Abilities.Any(a => a.Template.Type == ap.Type && a.IsOffCooldown))
            {
                validTypes.Add(ap.Type);

                if (Random.Range(0f, 1f) < ap.ChanceToSelect)
                {
                    return ap.Type;
                }
            }
        }
        if (validTypes.Count < 1)
        {
            return null;
        }
        return validTypes[Random.Range(0, validTypes.Count)];
    }

    private SigilAbility SelectAbility(GameUnit unit, SigilAbilityType type)
    {
        SigilAbility[] abilitiesOfType = unit.Abilities.Where(a => a.Template.Type == type && a.IsOffCooldown).ToArray();
        abilitiesOfType.Shuffle();
        return abilitiesOfType.Length > 0 ? abilitiesOfType[0] : null;
    }
}





