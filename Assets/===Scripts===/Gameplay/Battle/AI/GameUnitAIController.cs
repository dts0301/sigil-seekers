﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Controller")]
public class GameUnitAIController : ScriptableObject
{
    [SerializeField] GameUnitAIProfile defaultState;
    [SerializeField] GameUnitAIProfile whenThreatened;
    [SerializeField] GameUnitAIProfile whenThreatening;

    public void UpdateProfile(GameUnit unit, out GameUnitAIProfile profile)
    {
        if (unit.ThreatHandler.IsThreatened)
        {
            profile = whenThreatened;
        }
        else if (unit.ThreatHandler.IsThreatening)
        {
            profile = whenThreatening;
        }
        else
        {
            profile = defaultState;
        }
    }
}
