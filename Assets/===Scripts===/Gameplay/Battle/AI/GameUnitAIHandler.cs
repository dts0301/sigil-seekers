﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnitAIHandler : MonoBehaviour
{
    [SerializeField] private TurnController turnController;

    private GameUnitAIController controller;
    private GameUnitAIProfile profile;
    private GameUnit unit;
    private Dictionary<SigilAbilityType, List<SigilAbility>> abilities = new Dictionary<SigilAbilityType, List<SigilAbility>>();

    private void Awake()
    {
        unit = GetComponent<GameUnit>();
    }

    private void Start()
    {
        controller = unit.Template.AIController;
    }

    public void Initialize()
    {
        foreach (SigilAbility ab in unit.Abilities)
        {
            SigilAbilityType type = ab.Template.Type;
            if (type == null)
            {
                continue;
            }
            if (!abilities.ContainsKey(type))
            {
                abilities.Add(type, new List<SigilAbility>());
            }
            abilities[type].Add(ab);
        }
    }

    public void DoAITurn()
    {
        UpdateProfile();
        StartCoroutine(AITurnCoroutine());
    }

    private IEnumerator AITurnCoroutine()
    {
        while (profile.DoAction(unit))
        {
            yield return new WaitUntil(() => !unit.AbilitySystemAgent.AbilityHandler.Casting);
            yield return new WaitForSeconds(0.3f);
        }
        turnController.Next();
    }

    private void UpdateProfile()
    {
        controller.UpdateProfile(unit, out profile);
    }
}