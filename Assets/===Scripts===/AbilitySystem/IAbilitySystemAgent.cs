﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    public interface IAbilitySystemAgent
    {
        AbilityHandler AbilityHandler { get; }
        HealthHandler HealthHandler { get; }
        StatusHandler StatusHandler { get; }

        GameObject gameObject { get; }

        float CritChance { get; }
        float DodgeChance { get; }
    }
}