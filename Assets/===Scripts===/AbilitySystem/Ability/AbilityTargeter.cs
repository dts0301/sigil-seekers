﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Targeting/TargetingManager")]
    public class AbilityTargeter : ScriptableObject
    {        
        [SerializeField] private Texture2D targetingTexture;

        private List<IAbilitySystemAgent> selectedChars = new List<IAbilitySystemAgent>();
        private int numOfTargetsSelecting;
        private Predicate<IAbilitySystemAgent> targetChecker;
        private UnityAction<List<IAbilitySystemAgent>> onComplete;
        private UnityAction onCancel;

        private bool isTargeting;
        public bool IsTargeting
        {
            get { return isTargeting; }

            private set
            {
                isTargeting = value;
                if (value)
                {
                    Cursor.SetCursor(targetingTexture, 
                        new Vector2(targetingTexture.width / 2, targetingTexture.height / 2),
                        CursorMode.Auto);
                }
                else
                {
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                }
            }
        }

        public void SelectTarget(IAbilitySystemAgent selected)
        {
            if (IsTargeting == true && targetChecker != null && targetChecker(selected))
            {
                selectedChars.Add(selected);
                if (selectedChars.Count >= numOfTargetsSelecting)
                {
                    onComplete.Invoke(selectedChars);

                    selectedChars.Clear();
                    onComplete = null;
                    onCancel = null;
                    targetChecker = null;
                    numOfTargetsSelecting = 0;
                    IsTargeting = false;
                }
            }
        }

        public void ChooseNewTargets(int numberOfTargets, Predicate<IAbilitySystemAgent> checker,
            UnityAction<List<IAbilitySystemAgent>> onTargetingComplete, UnityAction onTargetingCancel)
        {
            selectedChars.Clear();
            numOfTargetsSelecting = numberOfTargets;
            targetChecker = checker;
            onComplete = onTargetingComplete;
            onCancel = onTargetingCancel;
            IsTargeting = true;
        }

        public void CancelTargeting()
        {
            if (onCancel != null)
            {
                onCancel.Invoke();
            }

            selectedChars.Clear();
            onComplete = null;
            onCancel = null;
            targetChecker = null;
            numOfTargetsSelecting = 0;
            IsTargeting = false;
        }
    }
}