﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/AbilityTemplate")]
    public class AbilityTemplate : ScriptableObject
    {
        [SerializeField] private ActionTemplate[] actions;
        public ActionTemplate[] Actions => actions;

        [SerializeField] private AbilityTargeting targeting;
        public AbilityTargeting Targeting => targeting;
    }
}