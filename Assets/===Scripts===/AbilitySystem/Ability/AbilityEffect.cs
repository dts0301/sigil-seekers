﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    public abstract class AbilityEffect : ScriptableObject
    {
        public abstract void TakeEffect(IAbilitySystemAgent caster, IAbilitySystemAgent target);
    }
}