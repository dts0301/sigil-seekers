﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class AbilityInstance
    {
        #region Object Pool
        private static int poolSize = 30;
        private static Stack<AbilityInstance> inactives = new Stack<AbilityInstance>();
        private static List<AbilityInstance> actives = new List<AbilityInstance>();

        static AbilityInstance()
        {
            for (int i = 0; i < poolSize; i++)
            {
                inactives.Push(new AbilityInstance());
            }
        }

        public static AbilityInstance GetInstance(AbilityTemplate ability = null, IAbilitySystemAgent caster = null)
        {
            if (inactives.Count == 0)
            {
                inactives.Push(new AbilityInstance());
            }
            AbilityInstance newInst = inactives.Pop();

            newInst.Template = ability;
            newInst.Caster = caster;
            newInst.Targets.Clear();
            return newInst;
        }
        #endregion

        public AbilityTemplate Template { get; private set; }
        public IAbilitySystemAgent Caster { get; private set; }
        public List<IAbilitySystemAgent> Targets { get; } = new List<IAbilitySystemAgent>();
        
        public bool CheckTargets()
        {
            return Targets.Count >= Template.Targeting.NumTargetsRequired;
        }

        public void Complete()
        {
            actives.Remove(this);
            inactives.Push(this);
        }
    }
    
    public class AbilityModifierSet : UnityEvent<AbilityInstance> { }
}
