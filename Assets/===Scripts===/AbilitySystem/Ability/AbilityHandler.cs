﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class AbilityHandler
    {
        public IAbilitySystemAgent Agent { get; private set; }
        public AbilityTargeter Targeter { get; private set; }
        
        public bool Casting { get; private set; }

        public AbilityModifierSet ModOnCastStart { get; private set; } = new AbilityModifierSet();
        public AbilityModifierSet ModOnCastFinish { get; private set; } = new AbilityModifierSet();
        public AbilityModifierSet ModOnCastedOnStart { get; private set; } = new AbilityModifierSet();

        public event UnityAction<ActionInstance> OnDoAction;
        public event UnityAction<AbilityInstance> OnCastBegin;
        public event UnityAction<AbilityInstance> OnCastCancel;
        public event UnityAction<AbilityInstance> OnCastDone;

        private AbilityInstance currentCast;
        private ActionInstance currentAction;
        private Queue<ActionInstance> actionQueue = new Queue<ActionInstance>();

        public AbilityHandler(IAbilitySystemAgent agent, AbilityTargeter targeter)
        {
            Agent = agent;
            Targeter = targeter;
        }

        public bool Cast(AbilityTemplate ability, List<IAbilitySystemAgent> targets = null)
        {
            currentCast = AbilityInstance.GetInstance(ability, Agent);

            //Auto Casting
            if (targets != null)
            {
                currentCast.Targets.AddRange(targets);
                if (!currentCast.CheckTargets())
                {
                    OnCastCancel?.Invoke(currentCast);
                    DoneCasting();
                    return false;
                }
                Execute();
            }
            //Auto Targeting Ability
            else if (ability.Targeting.IsAutoTarget)
            {
                ability.Targeting.AutoTarget(currentCast);
                if (!currentCast.CheckTargets())
                {
                    OnCastCancel?.Invoke(currentCast);
                    DoneCasting();
                    return false;
                }
                Execute();
            }
            // Manual Targeting Ability
            else
            {
                Targeter.ChooseNewTargets(
                    ability.Targeting.NumTargetsRequired,
                    selected => currentCast.Template.Targeting.SelectTargetCheck(selected, currentCast),
                    selection => { currentCast.Targets.AddRange(selection); Execute(); },
                    () => { OnCastCancel?.Invoke(currentCast); DoneCasting(); });
            }
            return true;
        }

        private void Execute()
        {
            OnCastBegin?.Invoke(currentCast);

            ModOnCastStart?.Invoke(currentCast);
            foreach (var target in currentCast.Targets)
            {
                target.AbilityHandler.ModOnCastedOnStart?.Invoke(currentCast);
            }

            ActionTemplate[] actions = currentCast.Template.Actions;
            List<IAbilitySystemAgent> targets = currentCast.Targets;

            foreach (ActionTemplate action in actions)
            {
                actionQueue.Enqueue(ActionInstance.GetInstance(action, Agent, targets));
            }
            if (!Casting)
            {
                DoNextAction();
            }
            Casting = true;
        }

        private void DoNextAction()
        {
            if (actionQueue.Count > 0)
            {
                ActionInstance action = actionQueue.Dequeue();
                currentAction = action;
                action.OnComplete += DoNextAction;
                OnDoAction?.Invoke(currentAction);
                HelperMethods.WaitThen(currentAction.Template.ActivationDelay, currentAction.DoAction);
            }
            else
            {
                DoneCasting();
            }
        }

        private void DoneCasting()
        {
            OnCastDone?.Invoke(currentCast);
            Casting = false;
            currentCast?.Complete();
            currentCast = null;
        }
    }
}