﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class ActionInstance
    {
        #region Object Pool

        private static int poolSize = 30;
        private static Stack<ActionInstance> inactives = new Stack<ActionInstance>();
        private static List<ActionInstance> actives = new List<ActionInstance>();

        static ActionInstance()
        {
            for (int i = 0; i < poolSize; i++)
            {
                inactives.Push(new ActionInstance());
            }
        }

        public static ActionInstance GetInstance(ActionTemplate template, IAbilitySystemAgent caster, List<IAbilitySystemAgent> targets)
        {
            if (inactives.Count == 0)
            {
                inactives.Push(new ActionInstance());
            }
            ActionInstance newInst = inactives.Pop();

            newInst.Template = template;
            newInst.Actor = caster;
            newInst.Targets = targets;
            newInst.OnComplete = null;
            return newInst;
        }
        #endregion

        public ActionTemplate Template { get; private set; }
        public IAbilitySystemAgent Actor { get; private set; }
        public List<IAbilitySystemAgent> Targets { get; private set; }

        public event UnityAction OnComplete;

        private float Delay => Template.DelayToNextAction;

        private ActionInstance() { }

        private ActionInstance(ActionTemplate actionTemplate, IAbilitySystemAgent actor, List<IAbilitySystemAgent> targets)
        {
            Template = actionTemplate;
            Actor = actor;
            Targets = targets;
        }

        public void DoAction()
        {
            DoActionEffects();
            DoActionSFXs();
            Timer.RunTimerOnce(Delay, OnComplete);

            actives.Remove(this);
            inactives.Push(this);
        }

        private void DoActionEffects()
        {
            if (Template.IsAoe)
            {
                for (int i = 0; i < Template.Effects.Length; i++)
                    for (int j = 0; j < Targets.Count; j++)
                        Template.Effects[i].TakeEffect(Actor, Targets[j]);
            }
            else
            {
                int indexOfTarget = Mathf.Clamp(Template.IndexOfPrimaryTarget, 0, Targets.Count - 1);
                for (int i = 0; i < Template.Effects.Length; i++)
                    Template.Effects[i].TakeEffect(Actor, Targets[indexOfTarget]);
            }
        }

        private void DoActionSFXs()
        {
            for (int i = 0; i < Template.CasterSfxs.Length; i++)
                Template.CasterSfxs[i].Value.GenerateSFX(Actor.gameObject.transform);

            if (Template.IsAoe)
            {
                for (int i = 0; i < Template.TargetSfxs.Length; i++)
                    for (int j = 0; j < Targets.Count; j++)
                        Template.TargetSfxs[i].Value.GenerateSFX(Targets[j].gameObject.transform);
            }
            else
            {
                int indexOfTarget = Mathf.Clamp(Template.IndexOfPrimaryTarget, 0, Targets.Count - 1);
                for (int i = 0; i < Template.TargetSfxs.Length; i++)
                    Template.TargetSfxs[i].Value.GenerateSFX(Targets[indexOfTarget].gameObject.transform);
            }
        }
    }

    public class ActionInstanceEvent : UnityEvent<ActionInstance> { }
}