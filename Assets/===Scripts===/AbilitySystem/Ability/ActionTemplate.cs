﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/ActionTemplate")]
    public class ActionTemplate : ScriptableObject
    {
        [SerializeField] private string animationName;
        public string AnimationName => animationName;

        [SerializeField] private float activationDelay;
        public float ActivationDelay => activationDelay;

        [SerializeField] private float delayToNextAction;
        public float DelayToNextAction => delayToNextAction;

        [Space]
        [SerializeField] private bool isAoe;
        public bool IsAoe => isAoe;

        [SerializeField] private int indexOfPrimaryTarget;
        public int IndexOfPrimaryTarget => indexOfPrimaryTarget;

        [Header("Functional Effects")]
        [SerializeField] private AbilityEffect[] effects;
        public AbilityEffect[] Effects => effects;

        [Header("Special Effects")]
        [SerializeField] private SpecialEffectGenerator[] targetSfxs;
        public SpecialEffectGenerator[] TargetSfxs => targetSfxs;

        [SerializeField] private SpecialEffectGenerator[] casterSfxs;
        public SpecialEffectGenerator[] CasterSfxs => casterSfxs;
    }
}