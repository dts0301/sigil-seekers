﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    public class SpecialEffectPool : MonoBehaviour
    {
        [SerializeField] private SpecialEffectGenerator reference;
        [SerializeField] private SpecialEffect sfxPrefab;

        private List<SpecialEffect> pool;
        private int poolIndex;
        private int poolSize = 10;

        private void Start()
        {
            reference.Value = this;

            pool = new List<SpecialEffect>();
            for (int i = 0; i < poolSize; i++)
            {
                pool.Add(Instantiate(sfxPrefab, transform));
            }
        }

        public SpecialEffect GenerateSFX(Transform attach)
        {
            SpecialEffect sfx = GetNext();
            if (sfx != null)
            {
                sfx.transform.position = attach.position;
                sfx.transform.SetParent(attach);
                sfx.Play();
            }
            return sfx;
        }

        private SpecialEffect GetNext()
        {
            int startingIndex = poolIndex;
            SpecialEffect result = pool[poolIndex];

            while (result.IsActive)
            {
                poolIndex = (poolIndex + 1).Wrap(0, pool.Count);
                result = pool[poolIndex];

                if (poolIndex == startingIndex)
                {
                    SpecialEffect newSfx = Instantiate(sfxPrefab, transform);
                    pool.Add(newSfx);
                    return newSfx;
                }
            }

            poolIndex = (poolIndex + 1).Wrap(0, pool.Count);
            return result;
        }
    }
}