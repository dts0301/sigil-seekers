﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/SFXPool")]
    public class SpecialEffectGenerator : ScriptableObject
    {
        public SpecialEffectPool Value { get; set; }
    }
}