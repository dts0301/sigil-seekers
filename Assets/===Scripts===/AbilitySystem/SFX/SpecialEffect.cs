﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    public class SpecialEffect : MonoBehaviour
    {
        [SerializeField] private bool persistent;

        private ParticleSystem particle;
        public bool IsActive { get; private set; }

        private void Start()
        {
            particle = GetComponent<ParticleSystem>();
        }

        public void Play()
        {
            particle.Play();
            if (persistent)
            {
                IsActive = true;
            }
        }

        public void Stop()
        {
            particle.Stop();
            if (persistent)
            {
                IsActive = false;
            }
        }
    }
}