﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Effects/Damage")]
    public class AbilityEffectDmg : AbilityEffect
    {
        [SerializeField] private EffectTarget effectTarget = EffectTarget.Target;
        [SerializeField] private float baseDamage;
        [SerializeField] private bool isAttack;
        [SerializeField] private AttackType attackType;
        [SerializeField] private DamageType damageType;
        [Header("Effects")]
        [SerializeField] private EffectAndTarget[] effectsOnHit;
        [Header("General Applicability")]
        [SerializeField] private bool checkMiss;
        [SerializeField] private bool mustMiss;
        [Space]
        [SerializeField] private bool checkDefended;
        [SerializeField] private bool mustBeDefended;
        [Space]
        [SerializeField] private bool checkCrit;
        [SerializeField] private bool mustCrit;
        [Header("Damage Instance Overrides")]
        [SerializeField] private float missChanceOverride = -1;
        [SerializeField] private float critChanceOverride = -1;
        [SerializeField] private float critMultiplierOverride = -1;

        [System.Serializable]
        private struct EffectAndTarget
        {
            public AbilityEffect Effect;
            public ApplicabilityOverrides ApplicableOverrides;
        }

        [System.Serializable]
        private struct ApplicabilityOverrides
        {
            public bool Used;
            [Space]
            public bool checkMiss;
            public bool isMiss;
            [Space]
            public bool checkDefended;
            public bool isDefended;
            [Space]
            public bool checkCrit;
            public bool isCrit;

            public bool CheckApplicable(DamageInstance dmgInst)
            {
                return (!checkMiss || dmgInst.IsMiss == isMiss) &&
                    (!checkDefended || dmgInst.IsDefended == isDefended) &&
                    (!checkCrit || dmgInst.IsCrit == isCrit);
            }
        }

        public override void TakeEffect(IAbilitySystemAgent caster, IAbilitySystemAgent target)
        {
            IAbilitySystemAgent victim = effectTarget == EffectTarget.Target ? target : caster;

            DamageInstance dmgInst = DamageInstance.GetInstance(caster, victim,
                baseDamage, isAttack, damageType, attackType, critChanceOverride, critMultiplierOverride, missChanceOverride);
            dmgInst.DoDamage();

            bool generallyApplicable =
                effectsOnHit.Length > 0 &&
                (!checkMiss || dmgInst.IsMiss == mustMiss) &&
                (!checkDefended || dmgInst.IsDefended == mustBeDefended) &&
                (!checkCrit || dmgInst.IsCrit == mustCrit);

            foreach (var effect in effectsOnHit)
            {
                if ((!effect.ApplicableOverrides.Used && generallyApplicable) || effect.ApplicableOverrides.CheckApplicable(dmgInst))
                {
                    effect.Effect.TakeEffect(caster, target);
                }    
            }
        }
    }
}