﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Types/DamageType")]
    public class DamageType : ScriptableObject
    {
        [SerializeField] Color color;
        public Color Color => color;
    }
}