﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class HealthHandler
    {
        public IAbilitySystemAgent Agent { get; private set; }

        public DamageInstanceEvent ModOnDealDamage { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnDealHealing { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnAttack { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnDefend { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnMiss { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnCrit { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnTakeDamage { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnTakeHealing { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnAttacked { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnDefended { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnMissed { get; private set; } = new DamageInstanceEvent();
        public DamageInstanceEvent ModOnCritted { get; private set; } = new DamageInstanceEvent();

        private float maxHealth;
        public float MaxHealth
        {
            get { return maxHealth; }

            private set
            {
                float difference = value - maxHealth;
                maxHealth = value;
                Health += difference;
                OnHealthChange?.Invoke();
            }
        }

        private float health;
        public float Health
        {
            get { return health; }

            private set
            {
                bool wasDead = health == 0;
                health = Mathf.Clamp(value, 0, MaxHealth);
                OnHealthChange?.Invoke();
                if (health == 0 && !wasDead)
                {
                    IsDead = true;
                }
                else if (wasDead)
                {
                    IsDead = false;
                }
            }
        }
        public event UnityAction OnHealthChange;

        private bool isDead;
        public bool IsDead { get { return isDead; } private set { isDead = value; OnDeathChange?.Invoke(value); } }
        public event UnityAction<bool> OnDeathChange;

        public HealthHandler(IAbilitySystemAgent agent)
        {
            Agent = agent;
        }

        public void Initialize(float maxHp)
        {
            MaxHealth = maxHp;
            Health = maxHp;
        }

        public void DealDamage(DamageInstance dmgInst)
        {
            if (IsDead)
            {
                return;
            }

            HealthHandler caster = dmgInst.Dealer.HealthHandler;
            HealthHandler target = dmgInst.Target.HealthHandler;
            
            if (!dmgInst.IsAttack || dmgInst.AttackType == null)
            {
                caster.ModOnDealDamage?.Invoke(dmgInst);
                target.ModOnTakeDamage?.Invoke(dmgInst);

                float finalDmg = dmgInst.Amount;
                target.Health -= Mathf.Max(0, finalDmg);
                return;
            }

            caster.ModOnAttack?.Invoke(dmgInst);
            target.ModOnAttacked?.Invoke(dmgInst);

            if (CheckDefended(dmgInst))
            {
                ModOnDefended?.Invoke(dmgInst);
                target.ModOnDefend?.Invoke(dmgInst);
                Debug.Log("Defended!");
            }

            if (CheckMiss(dmgInst))
            {
                caster.ModOnMiss?.Invoke(dmgInst);
                target.ModOnMissed?.Invoke(dmgInst);
                Debug.Log("Missed!");
            }
            else
            {
                if (CheckCrit(dmgInst))
                {
                    dmgInst.Amount *= dmgInst.CritMultiplierOverride != -1f ? dmgInst.CritMultiplierOverride : 2f;

                    caster.ModOnCrit?.Invoke(dmgInst);
                    target.ModOnCritted?.Invoke(dmgInst);
                    Debug.Log("Crit!");
                }
                caster.ModOnDealDamage?.Invoke(dmgInst);
                target.ModOnTakeDamage?.Invoke(dmgInst);

                float finalDmg = dmgInst.Amount;

                target.Health -= Mathf.Max(0, finalDmg);
            }
        }

        private bool CheckCrit(DamageInstance dmgInst)
        {
            return dmgInst.CanCrit &&
                (dmgInst.IsCrit || HelperMethods.CheckChance(dmgInst.CritChanceOverride != -1f ? dmgInst.CritChanceOverride : dmgInst.Dealer.CritChance));
        }

        private bool CheckMiss(DamageInstance dmgInst)
        {
            return dmgInst.CanMiss &&
                (dmgInst.IsMiss || HelperMethods.CheckChance(dmgInst.MissChanceOverride != -1f ? dmgInst.MissChanceOverride : dmgInst.Dealer.DodgeChance));
        }

        private bool CheckDefended(DamageInstance dmgInst)
        {
            return dmgInst.CanBeDefended && dmgInst.IsDefended;
        }
    }
}