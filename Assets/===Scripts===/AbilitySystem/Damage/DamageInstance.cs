﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class DamageInstance
    {
        #region Object Pooling
        private static int poolSize = 30;
        private static Stack<DamageInstance> inactives = new Stack<DamageInstance>();
        private static List<DamageInstance> actives = new List<DamageInstance>();

        static DamageInstance()
        {
            for (int i = 0; i < poolSize; i++)
            {
                inactives.Push(new DamageInstance());
            }
        }

        public static DamageInstance GetInstance(IAbilitySystemAgent dealer = null, IAbilitySystemAgent target = null, float damage = 0, bool isAttack = false,
            DamageType damageType = null, AttackType attackType = null, float critChanceOverride = -1f, float critMultiplierOverride = -1f, 
            float missChanceOverride = -1f)
        {
            if (inactives.Count == 0)
            {
                inactives.Push(new DamageInstance());
            }
            DamageInstance newInst = inactives.Pop();

            newInst.Dealer = dealer;
            newInst.Target = target;
            newInst.Amount = damage;
            newInst.IsAttack = isAttack;
            newInst.MissChanceOverride = missChanceOverride;
            newInst.CritChanceOverride = critChanceOverride;
            newInst.CritMultiplierOverride = critMultiplierOverride;
            newInst.AttackType = attackType;
            newInst.DamageType = damageType;
            newInst.IsCrit = false;
            newInst.IsDefended = false;
            newInst.IsMiss = false;
            newInst.CanCrit = true;
            newInst.CanBeDefended = true;
            newInst.CanMiss = true;

            return newInst;
        }
        #endregion

        public IAbilitySystemAgent Dealer;
        public IAbilitySystemAgent Target;
        public float Amount;
        public float MissChanceOverride;
        public float CritChanceOverride;
        public float CritMultiplierOverride;
        public bool IsAttack;
        public bool IsCrit;
        public bool IsDefended;
        public bool IsMiss;
        public bool CanCrit;
        public bool CanBeDefended;
        public bool CanMiss;
        public AttackType AttackType;
        public DamageType DamageType;

        public void DoDamage()
        {
            Dealer.HealthHandler.DealDamage(this);
            actives.Remove(this);
            inactives.Push(this);
        }
    }

    public class DamageInstanceEvent : UnityEvent<DamageInstance> { }
}