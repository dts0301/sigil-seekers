﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class StatusHandler
    {
        private List<StatusEffectInstance> statuses = new List<StatusEffectInstance>();

        public IAbilitySystemAgent Agent { get; private set; }

        public StatusInstanceEvent ModOnStatusApplyCaster { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusTickCaster { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusRemovedCaster { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusExpiredCaster { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusApplyTarget { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusTickTarget { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusRemovedTarget { get; private set; } = new StatusInstanceEvent();
        public StatusInstanceEvent ModOnStatusExpiredTarget { get; private set; } = new StatusInstanceEvent();

        public StatusHandler(IAbilitySystemAgent agent)
        {
            Agent = agent;
        }

        public void ApplyStatus(StatusEffectTemplate statusTemplate, IAbilitySystemAgent target)
        {
            if (!statusTemplate.AllowMultiple)
            {
                StatusEffectInstance preexistingStatus = target.StatusHandler.FindStatus(statusTemplate);
                if (preexistingStatus != null)
                {
                    preexistingStatus.Reset();
                    preexistingStatus.AddStack(1);

                    ModOnStatusApplyCaster.Invoke(preexistingStatus);
                    target.StatusHandler.ModOnStatusApplyTarget.Invoke(preexistingStatus);
                    return;
                }
            }
            StatusEffectInstance status = StatusEffectInstance.GetInstance(statusTemplate, Agent, target);
            target.StatusHandler.statuses.Add(status);
            status.OnApply();

            ModOnStatusApplyCaster.Invoke(status);
            target.StatusHandler.ModOnStatusApplyTarget.Invoke(status);
        }

        public void TickStatuses()
        {
            statuses.ForEach(status => 
            {
                if (status.CheckTick())
                {
                    status.Caster.StatusHandler.ModOnStatusTickCaster.Invoke(status);
                    status.Target.StatusHandler.ModOnStatusTickTarget.Invoke(status);
                }
            });
        }

        public void DecrementTimes(float timePassed)
        {
            statuses.ForEach(status => status.DecrementTime(timePassed));
            RemoveExpiredStatuses();
        }

        public void RemoveExpiredStatuses()
        {
            for (int i = statuses.Count - 1; i >= 0; i--)
            {
                StatusEffectInstance status = statuses[i];
                if (status.CurrentDuration <= 0.0005)
                {
                    status.OnRemove();
                    statuses.Remove(status);
                    status.Caster.StatusHandler.ModOnStatusExpiredCaster.Invoke(status);
                    status.Target.StatusHandler.ModOnStatusExpiredTarget.Invoke(status);
                }
            }
        }

        public void RemoveStatus(StatusEffectInstance status)
        {
            status.OnRemove();
            statuses.Remove(status);

            status.Caster.StatusHandler.ModOnStatusRemovedCaster.Invoke(status);
            status.Target.StatusHandler.ModOnStatusRemovedTarget.Invoke(status);
        }

        public StatusEffectInstance FindStatus(StatusEffectTemplate statusTemplate)
        {
            return statuses.FirstOrDefault(status => status.Template == statusTemplate);
        }

        public void ClearStatuses()
        {
            statuses.ForEach(s => RemoveStatus(s));
        }
    }
}