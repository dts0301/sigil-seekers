﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Effects/ApplyStatus")]
    public class AbilityEffectApplyStatus : AbilityEffect
    {
        [SerializeField] private EffectTarget effectTarget = EffectTarget.Target;
        [SerializeField] private StatusEffectTemplate statusTemplate;
        [SerializeField] private float chanceToApply = 1;

        public override void TakeEffect(IAbilitySystemAgent caster, IAbilitySystemAgent target)
        {
            if (chanceToApply == 1 || HelperMethods.CheckChance(chanceToApply))
            {
                caster.StatusHandler.ApplyStatus(statusTemplate, effectTarget == EffectTarget.Target ? target : caster);
            }
        }
    }
}