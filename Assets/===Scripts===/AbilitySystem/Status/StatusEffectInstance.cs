﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public class StatusEffectInstance
    {
        #region Object Pooling
        private static int poolSize = 30;
        private static Stack<StatusEffectInstance> inactives = new Stack<StatusEffectInstance>();
        private static List<StatusEffectInstance> actives = new List<StatusEffectInstance>();

        static StatusEffectInstance()
        {
            for (int i = 0; i < poolSize; i++)
            {
                inactives.Push(new StatusEffectInstance());
            }
        }

        public static StatusEffectInstance GetInstance(StatusEffectTemplate template = null, IAbilitySystemAgent caster = null, IAbilitySystemAgent target = null)
        {
            if (inactives.Count == 0)
            {
                inactives.Push(new StatusEffectInstance());
            }
            StatusEffectInstance newInst = inactives.Pop();

            newInst.Template = template;
            newInst.Caster = caster;
            newInst.Target = target;
            newInst.Data.Reset();
            newInst.modifierList.Clear();
            newInst.persistentSfxList.Clear();
            return newInst;
        }
        #endregion

        public StatusEffectTemplate Template { get; private set; }
        public IAbilitySystemAgent Caster { get; private set; }
        public IAbilitySystemAgent Target { get; private set; }
        public StatusEffectData Data { get; private set; } = new StatusEffectData();
        public float CurrentDuration { get; private set; }
        public float CurrentTickTime { get; private set; }
        public int Stacks { get; private set; }

        private List<Modifier> modifierList = new List<Modifier>();
        private List<SpecialEffect> persistentSfxList = new List<SpecialEffect>();

        public void OnApply()
        {
            CurrentDuration = Template.Duration;
            CurrentTickTime = Template.TickRate;
            Stacks = Template.StartingStacks;

            DoEffects(Template.OnApply);
            DoSFXs(Template.OnApplySFX);
            
            foreach (ModifierTemplate mod in Template.Modifiers)
            {
                modifierList.Add(mod.GenerateInstance());
            }
            AddModifiers();

            foreach (SpecialEffectGenerator sfxRef in Template.PersistentSFX)
            {
                SpecialEffect sfx = sfxRef.Value.GenerateSFX(Target.gameObject.transform);
                persistentSfxList.Add(sfx);
            }
        }

        public bool CheckTick()
        {
            if (CurrentTickTime <= 0)
            {
                DoEffects(Template.OnTick);
                DoSFXs(Template.OnTickSFX);
                CurrentTickTime = Template.TickRate;
                return true;
            }
            return false;
        }

        public void DecrementTime(float timePassed)
        {
            CurrentTickTime -= timePassed;
            CurrentDuration -= timePassed;
        }

        public void OnRemove()
        {
            CurrentDuration = 0;
            DoEffects(Template.OnRemove);
            DoSFXs(Template.OnRemoveSFX);
            RemoveModifiers();

            modifierList.Clear();
            foreach (SpecialEffect sfx in persistentSfxList)
            {
                sfx.Stop();
            }

            actives.Remove(this);
            inactives.Push(this);
        }

        public void ForceRemove()
        {
            Target.StatusHandler.RemoveStatus(this);
        }

        public void Reset()
        {
            CurrentDuration = Template.Duration;
            CurrentTickTime = Template.TickRate;
        }

        public void AddStack(int stacks, bool refreshTime = true)
        {
            if (refreshTime)
            {
                Reset();
            }
            Stacks = Mathf.Min(Stacks + stacks, Template.MaxStacks);
        }

        public void RemoveStack()
        {
            Stacks--;
            if (Stacks == 0)
            {
                ForceRemove();
            }
        }

        private void DoEffects(AbilityEffect[] effectList)
        {
            foreach (AbilityEffect effect in effectList)
            {
                effect.TakeEffect(Caster, Target);
            }
        }

        private void RemoveModifiers()
        {
            foreach (Modifier mod in modifierList)
            {
                mod.Remove();
            }
        }

        private void AddModifiers()
        {
            foreach (Modifier mod in modifierList)
            {
                mod.Apply(Target, this);
            }
        }

        private void DoSFXs(SpecialEffectGenerator[] sfxList)
        {
            foreach (SpecialEffectGenerator sfxRef in sfxList)
            {
                SpecialEffect sfx = sfxRef.Value.GenerateSFX(Target.gameObject.transform);
            }
        }

        public class StatusEffectData
        {
            Dictionary<string, float> floatData = new Dictionary<string, float>();

            public float GetFloat(string name)
            {
                return floatData.ContainsKey(name) ? floatData[name] : float.NaN;
            }

            public void Reset()
            {
                floatData.Clear();
            }
        }
    }
    
    public class StatusInstanceEvent : UnityEvent<StatusEffectInstance> { }
}