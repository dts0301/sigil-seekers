﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/StatusTemplate")]
    public class StatusEffectTemplate : ScriptableObject
    {
        [SerializeField] private float duration = 1;
        [SerializeField] private float tickRate;
        [SerializeField] private bool allowMultiple;
        [SerializeField] private int maxStacks = 1;
        [SerializeField] private int startingStacks = 1;
        [Header("Functional Effects")]
        [SerializeField] private ModifierTemplate[] modifiers;
        [Space]
        [SerializeField] private AbilityEffect[] onApply;
        [SerializeField] private AbilityEffect[] onRemove;
        [SerializeField] private AbilityEffect[] onTick;
        [Header("Special Effects")]
        [SerializeField] private SpecialEffectGenerator[] persistentSfx;
        [SerializeField] private SpecialEffectGenerator[] onApplySfx;
        [SerializeField] private SpecialEffectGenerator[] onTickSfx;
        [SerializeField] private SpecialEffectGenerator[] onRemoveSfx;

        public float Duration => duration;
        public float TickRate => tickRate;
        public bool AllowMultiple => allowMultiple;
        public int MaxStacks => maxStacks;
        public int StartingStacks => startingStacks;
        public ModifierTemplate[] Modifiers => modifiers;
        public AbilityEffect[] OnApply => onApply;
        public AbilityEffect[] OnRemove => onRemove;
        public AbilityEffect[] OnTick => onTick;
        public SpecialEffectGenerator[] PersistentSFX => persistentSfx;
        public SpecialEffectGenerator[] OnApplySFX => onApplySfx;
        public SpecialEffectGenerator[] OnTickSFX => onTickSfx;
        public SpecialEffectGenerator[] OnRemoveSFX => onRemoveSfx;
    }
}