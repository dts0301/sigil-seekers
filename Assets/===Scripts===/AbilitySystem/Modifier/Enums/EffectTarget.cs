﻿
namespace SkySeekers.AbilitySystem
{
    public enum EffectTarget
    {
        Origin, Caster, Target
    }
}