﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public abstract class ModifierTemplate : ScriptableObject
    {
        public abstract Modifier GenerateInstance();
    }
}