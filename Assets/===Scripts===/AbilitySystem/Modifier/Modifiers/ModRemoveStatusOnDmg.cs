﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Modifiers/RemoveStatusOnDamage")]
    public class ModRemoveStatusOnDmg : ModifierTemplate
    {
        [SerializeField] private IncomingOrOutgoing incomingOrOutgoing;
        [SerializeField] private float chance;
        [SerializeField] private DamageType damageType;
        [SerializeField] private AttackType attackType;
        [SerializeField] private bool mustBeAttack, attackMustHit, attackMustBeDefended, attackMustCrit;

        public override Modifier GenerateInstance()
        {
            return new ModRemoveStatusOnAttack(this);
        }

        private class ModRemoveStatusOnAttack : Modifier<DamageInstance, ModRemoveStatusOnDmg>
        {
            public ModRemoveStatusOnAttack(ModRemoveStatusOnDmg template) : base(template)
            {
                Template = template;
            }

            protected override UnityEvent<DamageInstance> GetEvent(IAbilitySystemAgent target)
            {
                return Template.incomingOrOutgoing == IncomingOrOutgoing.Incoming ? target.HealthHandler.ModOnTakeDamage : target.HealthHandler.ModOnDealDamage;
            }

            protected override void Modify(DamageInstance instance)
            {
                if ((Template.chance == 1 || HelperMethods.CheckChance(Template.chance)) &&
                    DetermineApplicable(instance))
                {
                    OriginStatusEffect.ForceRemove();
                }
            }

            private bool DetermineApplicable(DamageInstance instance)
            {
                if (Template.damageType != null && instance.DamageType != Template.damageType)
                {
                    return false;
                }
                if (Template.mustBeAttack)
                {
                    if (!instance.IsAttack)
                    {
                        return false;
                    }
                    if (Template.attackMustHit && instance.IsMiss)
                    {
                        return false;
                    }
                    if (Template.attackMustBeDefended && !instance.IsDefended)
                    {
                        return false;
                    }
                    if (Template.attackMustCrit && !instance.IsCrit)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}