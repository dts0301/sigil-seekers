﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Modifiers/DamageModifier")]
    public class ModAttack : ModifierTemplate
    {
        private enum OnAttackOrOnDamage { OnDamage, OnAttack }

        [SerializeField] private IncomingOrOutgoing incomingOrOutgoing;
        [SerializeField] private OnAttackOrOnDamage modifyWhen;
        [SerializeField] private float chanceToApply = 1f;
        [SerializeField] private bool consumesStatus = false;
        [Space]
        [SerializeField] private float damageScaler = 1f;
        [SerializeField] private float damageConstantScaler;
        [Space]
        [SerializeField] private bool modifiesMiss;
        [SerializeField] private bool miss;
        [Space]
        [SerializeField] private bool modifiesCrit;
        [SerializeField] private bool crit;
        [Space]
        [SerializeField] private bool modifiesDefended;
        [SerializeField] private bool defended;
        [Header("Applicable Instances")]
        [SerializeField] private DamageType damageType;
        [SerializeField] private AttackType attackType;
        [Space]
        [SerializeField] private bool checkAttack;
        [SerializeField] private bool mustBeAttack;
        [Space]
        [SerializeField] private bool checkMiss;
        [SerializeField] private bool mustBeMiss;
        [Space]
        [SerializeField] private bool checkDefended;
        [SerializeField] private bool mustBeDefended;
        [Space]
        [SerializeField] private bool checkCrit;
        [SerializeField] private bool mustBeCrit;

        public override Modifier GenerateInstance()
        {
            return new ModDamageReduction(this);
        }

        private class ModDamageReduction : Modifier<DamageInstance, ModAttack>
        {
            public ModDamageReduction(ModAttack template) : base(template) { }

            protected override void Modify(DamageInstance instance)
            {
                if (HelperMethods.CheckChance(Template.chanceToApply) &&
                    DetermineApplicable(instance))
                {
                    instance.Amount *= Template.damageScaler;
                    instance.Amount += Template.damageConstantScaler;

                    if (Template.modifiesMiss)
                    {
                        instance.IsMiss = Template.miss;
                    }
                    if (Template.modifiesCrit)
                    {
                        instance.IsCrit = Template.crit;
                    }
                    if (Template.modifiesDefended)
                    {
                        instance.IsDefended = Template.defended;
                    }
                    if (Template.consumesStatus)
                    {
                        OriginStatusEffect.RemoveStack();
                    }
                }
            }

            protected override UnityEvent<DamageInstance> GetEvent(IAbilitySystemAgent target)
            {
                return Template.modifyWhen == OnAttackOrOnDamage.OnAttack ? 
                    (Template.incomingOrOutgoing == IncomingOrOutgoing.Outgoing ? target.HealthHandler.ModOnAttack : target.HealthHandler.ModOnAttacked) :
                    (Template.incomingOrOutgoing == IncomingOrOutgoing.Outgoing ? target.HealthHandler.ModOnDealDamage : target.HealthHandler.ModOnTakeDamage);
            }

            private bool DetermineApplicable(DamageInstance instance)
            {
                if ((Template.damageType != null && instance.DamageType != Template.damageType) ||
                    (Template.attackType != null && instance.AttackType != Template.attackType) ||
                    (Template.checkAttack && Template.mustBeAttack != instance.IsAttack) ||
                    (Template.checkMiss && Template.mustBeMiss != instance.IsMiss) ||
                    (Template.checkDefended && Template.mustBeDefended != instance.IsDefended) ||
                    (Template.checkCrit && Template.mustBeCrit != instance.IsCrit))
                {
                    return false;
                }

                return true;
            }
        }
    }
}