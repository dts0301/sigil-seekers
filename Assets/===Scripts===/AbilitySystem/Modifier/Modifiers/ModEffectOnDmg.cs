﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    [CreateAssetMenu(menuName = "AbilitySystem/Modifiers/EffectOnDamage")]
    public class ModEffectOnDmg : ModifierTemplate
    {
        [SerializeField] private IncomingOrOutgoing incomingOrOutgoing;
        [SerializeField] private EffectTarget effectTarget;
        [SerializeField] private AbilityEffect[] effects;
        [SerializeField] private float chance = 1;
        [SerializeField] private bool consumeOnUse = false;
        [Header("Applicable Instances")]
        [SerializeField] private DamageType damageType;
        [SerializeField] private AttackType attackType;
        [SerializeField] private bool mustBeAttack, attackMustHit, attackMustBeDefended, attackMustCrit;

        public override Modifier GenerateInstance()
        {
            return new ModEffectOnAttacking(this);
        }

        private class ModEffectOnAttacking : Modifier<DamageInstance, ModEffectOnDmg>
        {
            public ModEffectOnAttacking(ModEffectOnDmg template) : base(template)
            {
                Template = template;
            }

            protected override UnityEvent<DamageInstance> GetEvent(IAbilitySystemAgent target)
            {
                return Template.incomingOrOutgoing == IncomingOrOutgoing.Outgoing ? target.HealthHandler.ModOnDealDamage : target.HealthHandler.ModOnTakeDamage;
            }

            protected override void Modify(DamageInstance instance)
            {
                if ((Template.chance == 1 || HelperMethods.CheckChance(Template.chance)) &&
                    DetermineApplicable(instance))
                {
                    IAbilitySystemAgent eftTrgt;
                    switch (Template.effectTarget)
                    {
                        case EffectTarget.Origin:
                            eftTrgt = OriginStatusEffect.Caster;
                            break;
                        case EffectTarget.Caster:
                            eftTrgt = instance.Dealer;
                            break;
                        default:
                            eftTrgt = instance.Target;
                            break;
                    }
                    for (int i = 0; i < Template.effects.Length; i++)
                    {
                        Template.effects[i].TakeEffect(OriginStatusEffect.Caster, eftTrgt);
                    }

                    if (Template.consumeOnUse)
                    {
                        OriginStatusEffect.RemoveStack();
                    }
                }
            }

            private bool DetermineApplicable(DamageInstance instance)
            {
                if (Template.damageType != null && instance.DamageType != Template.damageType)
                {
                    return false;
                }
                if (Template.mustBeAttack)
                {
                    if (!instance.IsAttack)
                    {
                        return false;
                    }
                    if (Template.attackMustHit && instance.IsMiss)
                    {
                        return false;
                    }
                    if (Template.attackMustBeDefended && !instance.IsDefended)
                    {
                        return false;
                    }
                    if (Template.attackMustCrit && !instance.IsCrit)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

    }
}