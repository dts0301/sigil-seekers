﻿
using UnityEngine.Events;

namespace SkySeekers.AbilitySystem
{
    public abstract class Modifier
    {
        public abstract void Apply(IAbilitySystemAgent character, StatusEffectInstance origin = null);

        public abstract void Remove();
    }

    public abstract class Modifier<T, K> : Modifier where K : ModifierTemplate
    {
        public K Template { get; protected set; }
        public IAbilitySystemAgent Target { get; private set; }
        public StatusEffectInstance OriginStatusEffect { get; private set; }

        public Modifier(K template)
        {
            Template = template;
        }

        protected abstract UnityEvent<T> GetEvent(IAbilitySystemAgent target);

        public override void Apply(IAbilitySystemAgent target, StatusEffectInstance origin = null)
        {
            Target = target;
            OriginStatusEffect = origin;
            UnityEvent<T> evnt = GetEvent(target);
            evnt.AddListener(Modify);
        }

        public override void Remove()
        {
            UnityEvent<T> evnt = GetEvent(Target);
            evnt.RemoveListener(Modify);
        }

        protected abstract void Modify(T instance);
    }

}