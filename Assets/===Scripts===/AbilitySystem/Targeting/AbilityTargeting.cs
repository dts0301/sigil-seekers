﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkySeekers.AbilitySystem
{
    public abstract class AbilityTargeting : ScriptableObject
    {
        [SerializeField] private int requiredTargets = 1;
        [SerializeField] private protected bool cannotSelectSelf, canSelectDead, mustSelectDead, cannotRepeatSelection;

        public virtual bool IsAutoTarget => false;
        public int NumTargetsRequired => requiredTargets;

        public virtual void AutoTarget(AbilityInstance abilityInstance) { }

        public virtual bool SelectTargetCheck(IAbilitySystemAgent selected, AbilityInstance abilityInstance)
        {
            if (selected == null)
            {
                return false;
            }

            GameObject caster = abilityInstance.Caster.gameObject;
            return
                (canSelectDead || !selected.HealthHandler.IsDead) &&
                (!mustSelectDead || selected.HealthHandler.IsDead) &&
                (!cannotSelectSelf || selected.gameObject != caster) &&
                (!cannotRepeatSelection || !abilityInstance.Targets.Contains(selected));
        }

    }
}