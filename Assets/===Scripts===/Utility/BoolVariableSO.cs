﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Utility/BoolVariable")]
public class BoolVariableSO : ObservedSOVariable<bool> { }