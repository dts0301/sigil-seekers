﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Utility/FloatVariable")]
public class FloatVariableSO : ObservedSOVariable<float> { }