﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class IntVariable : ObservedVariable<int> { }

[Serializable]
public class IntEvent : UnityEvent<int> { }