﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Utility/IntVariable")]
public class IntVariableSO : ObservedSOVariable<int> { }
