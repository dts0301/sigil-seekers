﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class BoolVariable : ObservedVariable<bool> { }

[Serializable]
public class BoolEvent : UnityEvent<bool> { }
