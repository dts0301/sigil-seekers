﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObservedVariable<T>
{
    private T _value;
    public T Value { get { return _value; } set { prechange?.Invoke(); _value = value; postchange?.Invoke(); } }

    [SerializeField] private UnityEvent prechange;
    [SerializeField] private UnityEvent postchange;

    public void AddPreListener(UnityAction action) => prechange.AddListener(action);
    public void RemovePreListener(UnityAction action) => prechange.RemoveListener(action);
    public void AddPostListener(UnityAction action) => postchange.AddListener(action);
    public void RemovePostListener(UnityAction action) => postchange.AddListener(action);
}
