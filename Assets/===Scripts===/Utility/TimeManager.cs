﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        Timer.UpdateActiveTimers();
    }
}

public class Timer
{
    private static int poolSize = 30;
    private static Stack<Timer> inactiveTimers = new Stack<Timer>();
    private static List<Timer> activeTimers = new List<Timer>();

    static Timer()
    {
        for (int i = 0; i < poolSize; i++)
        {
            inactiveTimers.Push(new Timer());
        }
    }

    public static void UpdateActiveTimers()
    {
        for (int i = 0; i < activeTimers.Count; i++)
        {
            Timer timer = activeTimers[i];
            timer.Update();
        }
    }

    public static void RunTimerOnce(float interval = 1, UnityAction onComplete = null)
    {
        if (inactiveTimers.Count == 0)
        {
            inactiveTimers.Push(new Timer());
        }
        Timer timer = inactiveTimers.Pop();

        timer.OnComplete.RemoveAllListeners();
        if (onComplete != null)
        {
            timer.OnComplete.AddListener(onComplete);
        }

        timer.Interval = interval;
        timer.Looping = false;
        timer.time = 0;
        timer.IsRunning = true;
        timer.DisposeOnComplete = true;

        activeTimers.Add(timer);
    }

    public static Timer GetInstance(float interval = 1, UnityAction onComplete = null, bool looping = false)
    {
        if (inactiveTimers.Count == 0)
        {
            inactiveTimers.Push(new Timer());
        }
        Timer timer = inactiveTimers.Pop();

        timer.OnComplete.RemoveAllListeners();
        if (onComplete != null)
        {
            timer.OnComplete.AddListener(onComplete);
        }

        timer.Interval = interval;
        timer.Looping = looping;
        timer.IsRunning = false;
        timer.DisposeOnComplete = false;
        timer.time = 0;

        activeTimers.Add(timer);
        return timer;
    }

    private float time;
    private float interval;
    public float Interval { get { return interval; } set { interval = Mathf.Max(value, 0); time = 0; } }
    public bool IsRunning { get; private set; }
    public bool Looping { get; set; }
    public bool DisposeOnComplete { get; set; }

    private UnityEvent OnComplete = new UnityEvent();
    public void AddOnCompleteListener(UnityAction listener) => OnComplete.AddListener(listener);
    public void RemoveOnCompleteListener(UnityAction listener) => OnComplete.RemoveListener(listener);
    
    private Timer() { }

    public void Update()
    {
        if (!IsRunning)
        {
            return;
        }

        time += Time.deltaTime;
        if (time >= Interval)
        {
            if (Looping)
            {
                time -= Interval;
                OnComplete?.Invoke();
            }
            else
            {
                OnComplete?.Invoke();
                Stop();
                Reset();
                if (DisposeOnComplete)
                {
                    Dispose();
                }
            }
        }
    }

    public void Start()
    {
        IsRunning = true;
    }

    public void Reset()
    {
        time = 0;
    }

    public void Stop()
    {
        IsRunning = false;
    }

    public void Dispose()
    {
        OnComplete.RemoveAllListeners();
        IsRunning = false;

        activeTimers.Remove(this);
        inactiveTimers.Push(this);
    }
}
