﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public static class HelperMethods
{
    public static void Shuffle<T>(this IList<T> list)
    {
        for (int i = list.Count - 1; i > 1; i--)
        {
            int rnd = UnityEngine.Random.Range(0, i + 1);

            T value = list[rnd];
            list[rnd] = list[i];
            list[i] = value;
        }
    }

    public static float Wrap(this float targetVal, float lowerBound, float upperBound)
    {
        float range = upperBound - lowerBound;
        if (targetVal > upperBound)
        {
            float diff = targetVal - upperBound;
            return lowerBound + (diff % range);
        }
        else if (targetVal < lowerBound)
        {
            float diff = lowerBound - targetVal;
            return upperBound - (diff % range);
        }
        return targetVal;
    }

    public static int Wrap(this int targetVal, int lowerBound, int upperBound)
    {
        int range = upperBound - lowerBound;
        if (targetVal > upperBound)
        {
            int diff = targetVal - upperBound;
            return lowerBound + (diff % range);
        }
        else if (targetVal < lowerBound)
        {
            int diff = lowerBound - targetVal;
            return upperBound - (diff % range);
        }
        return targetVal;
    }

    public static bool RaycastAtMouse(Camera camera, out RaycastHit hit)
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            return true;
        }
        return false;
    }

    public static bool RaycastAtMouse(Camera camera, LayerMask layer, out RaycastHit hit)
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit, 100, layer);
        
    }

    public static bool RaycastAtMouse2D(Camera camera, LayerMask layer, out RaycastHit2D hit)
    {
        hit = Physics2D.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 100, layer);
        return hit.collider != null;
    }

    public static bool CheckChance(float chance)
    {
        return UnityEngine.Random.Range(0, 1f) <= chance;
    }

    public static void WaitThen(float howLong, UnityAction action)
    {
        Timer.GetInstance(howLong, action, false).Start();
    }

    public static Timer Loop(float interval, UnityAction action)
    {
        Timer result = Timer.GetInstance(interval, action, true);
        result.Start();
        return result;
    }

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}