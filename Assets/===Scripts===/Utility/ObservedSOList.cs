﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;

public abstract class ObservedSOList<T> : ScriptableObject
{
    private ObservableCollection<T> list = new ObservableCollection<T>();
    public ICollection Items { get => list; }

    private event UnityAction<IList> elementsAdded;
    private event UnityAction<IList> elementsRemoved;
    private event UnityAction cleared;

    public ObservedSOList()
    {
        list.CollectionChanged += Changed;
    }

    private void Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
        switch (e.Action)
        {
            case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                elementsAdded?.Invoke(e.NewItems);
                break;
            case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                elementsRemoved?.Invoke(e.OldItems);
                break;
            case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                cleared?.Invoke();
                break;
            default:
                break;
        }
    }

    public void AddAddedListener(UnityAction<IList> action) => elementsAdded += action;
    public void RemoveAddedListener(UnityAction<IList> action) => elementsAdded -= action;
    public void AddRemovedListener(UnityAction<IList> action) => elementsRemoved += action;
    public void RemoveRemovedListener(UnityAction<IList> action) => elementsRemoved -= action;
    public void AddClearedListener(UnityAction action) => cleared += action;
    public void RemoveClearedListener(UnityAction action) => cleared -= action;
}
