﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class FloatVariable : ObservedVariable<float> { }

[Serializable]
public class FloatEvent : UnityEvent<float> { }
